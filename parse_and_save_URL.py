#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 10:53:04 2021

@author: cybermeme
"""

### Librairies a importer ###
import requests
import re
from bs4 import BeautifulSoup
from datetime import datetime


### Constantes et variables ###
url_to_search = "http://hackycorp.com"
regex2_to_hunt = "^[0-9a-zA-Z]+\.[0-9a-zA-Z]+"

destination_file_internal = "/tmp/URL_parsed_internal.txt"
destination_file_external = "/tmp/URL_parsed_external.txt"
destination_file_ajout = "/tmp/URL_parsed_ajout.txt"
delimiter = '/'

liste_full_internal = []
liste_full_external = []
liste_full_ajout = []

now = datetime.now()
Timestamp = "Timestamp - " + now.strftime('%Y_%m_%d_%a-%H_%M_%S')

### Definitions ###
def check_url(url_to_check):
    regex_url = re.findall(regex2_to_hunt,url_to_check)
    url = clean_line(url_to_check,delimiter)
    if url == "":
        return ""
    if url.startswith('http') or url.startswith('http') or regex_url:
        if not url.startswith("http"):
            if url not in liste_full_ajout:
                liste_full_ajout.append(url)
                save_in_file(url, destination_file_ajout)
            url = "https://" + url
        if url not in liste_full_external:
            liste_full_external.append(url)
            save_in_file(url, destination_file_external)
        return "external : " + url_to_check
    else:
        if url not in liste_full_internal:
            liste_full_internal.append(url)
            save_in_file(url, destination_file_internal)
        return url_to_check

def open_parse(url_to_parse):
    url = check_url(url_to_parse)
    save_in_file(clean_line(url,delimiter), destination_file)
    page_temp = requests.get(url).text
    parsage_regex(page_temp)

def parsage_url(tag,location):
    for z in soup(location):
        try:
            tampon = (z[tag].lstrip('/#'))
            if tampon:
                open_parse(tampon)
        except:
            pass

def save_in_file(data, backupFile):
    try:
        f = open(backupFile, "a")
        f.write(data + "\n")
        f.close()
    except:
        return 1
    return 0


def clean_line(data,delimiter):
    tampon = data.split(delimiter)
    tampon.pop()
    sanitized = ""
    for x in tampon:
        sanitized = sanitized + x + delimiter
    return sanitized



### main ###
page = requests.get(url_to_search)
soup = BeautifulSoup(page.content, 'html.parser')

print("[-] Started".center(40,"*"))
save_in_file(Timestamp.center(80,'*'), destination_file_ajout)
save_in_file(Timestamp.center(80,'*'), destination_file_internal)
save_in_file(Timestamp.center(80,'*'), destination_file_external)

parsage_url("src","script")
parsage_url("src","img")
parsage_url("href","a")
parsage_url("href","link")

print("[-] Finished".center(40,"*"))
