#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 09:09:33 2021

@author: cybermeme


Quick and dirty
"""

import os
import sys


### Functions ###
def save_in_file(data, backupFile):
    try:
        f = open(backupFile, "a")
        f.write(data + "\n")
        f.close()
    except:
        return 1
    return 0


def load_from_file():
    try:
        f = open(sys.argv[1], "r")
        [ save_in_file(clean_line(x,sys.argv[3]),sys.argv[2]) for x in f.readlines() ]
        f.close()
    except:
        return 1


def clean_line(data,delimiter):
    tampon = data.split(delimiter)
    tampon.pop()
    sanitized = ""
    for x in tampon:
        sanitized = sanitized + x + delimiter
    return sanitized


### main ###
if len(sys.argv) == 4:
    load_from_file()
else:
    print('''Usage: ScriptName file2load file2save delimiter
          ex: python ./URL_sanitize_last_part.py /tmp/lst /tmp/liste_sani /
              Will remove everything avec the last "/" of every line in the
              /tmp/lst and will add in the file /tmp/liste_sani''')