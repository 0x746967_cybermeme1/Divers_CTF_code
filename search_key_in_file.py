#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 10:53:04 2021

@author: cybermeme
"""

### Librairies a importer ###
import sys
import requests
import re
from bs4 import BeautifulSoup


### Constantes et variables ###
url_to_search = "http://superUrl.fqdn"
regex_to_hunt = "[0-9a-f]+\-[0-9a-f]+\-[0-9a-f]+\-[0-9a-f]+\-[0-9a-f]+"


### Definitions ###
def check_url(url_to_check):
    if url_to_check.startswith('http'):
        return url_to_check
    else:
        return 'http://' + url_to_check

def open_parse(url_to_parse):
    url = check_url(url_to_parse)
    print("[+] URL : " + url)
    page_temp = requests.get(url).text
    parsage_regex(page_temp)

def parsage_url(tag,location):
    for z in soup(location):
        try:
            tampon = (z[tag].lstrip('/#'))
            if tampon:
                open_parse(tampon)
        except:
            pass

def parsage_regex(text_to_hunt):
    x = re.findall(regex_to_hunt,text_to_hunt)
    if x:
        print("\t[-] Results found : ", x)

### main ###
page = requests.get(url_to_search)
soup = BeautifulSoup(page.content, 'html.parser')


parsage_url("src","script")
#parsage_url("src","img")
#parsage_url("href","a")
#parsage_url("href","link")
    

