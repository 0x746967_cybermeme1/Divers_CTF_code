#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cybermeme

exercice from mooc https://learning.edx.org/course/course-v1:IsraelX+infosec102+3T2020/home
"""

from struct import *
packet = b'\x08\x00\x00\x00\xf6\x01\x00\x00\x24\x00\x00\x00\x03\x00\x00\x00\x0c\x00\x00\x00I think, therefore I am.\xca\xcd\x00\x00'

#### Don't change the code until this line ####

def show_details():
    analysepacket = unpack('3I I I 24s I', packet)
    listePacket = []
    [ listePacket.append(c) for c in analysepacket]

    print (int(listePacket[0]),int(listePacket[4]),listePacket[5].decode('ascii'),int(listePacket[6]))


show_details()
