#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cybermeme
exercice from mooc https://learning.edx.org/course/course-v1:IsraelX+infosec102+3T2020/home
"""

from scapy.all import *
from scapy.layers.http import *
import sys # ignore
sys.path.insert(0,'.') # ignore
from create_recording import recording_path # the path to the pcap file of this assignment

#### Don't change the code until this line ####

def show_source_destination_ip_address():
    packets = rdpcap(recording_path)
    packets[3].show() #full packet n°4
    print(packets[3][IP].src) #packet n°4 IP src
    print(packets[0][IP].dst) #packet n°1 IP dest


show_source_destination_ip_address()
